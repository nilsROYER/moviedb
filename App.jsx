import React from "react";
import { StyleSheet, ScrollView, View } from "react-native";
import MovieRouter from "./routers/MovieRouter";

const App = () => {
  return (
    <MovieRouter/>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default App;
