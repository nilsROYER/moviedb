import React from "react";
import { StyleSheet, View } from "react-native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "../screens/Home";
import DetailMovieScreen from "../screens/DetailMovieScreen";
import SearchMovieScreen from "../screens/SearchMovieScreen";
import { NavigationContainer } from "@react-navigation/native";

const { Navigator, Screen } = createNativeStackNavigator();
const MovieRouter = () => {
  return (
    <NavigationContainer>
      <Navigator
        screenOptions={{
          headerShown: true,
          headerTransparent: true,
          headerShadowVisible: false,
        }}
      >
        <Screen name="Home" component={Home} />
        <Screen
          name="DetailMovieScreen"
          component={DetailMovieScreen}
          options={{ title: "Movie detail" }}
        />
        <Screen
          name="SearchMovieScreen"
          component={SearchMovieScreen}
          options={{ title: "Search" }}
        />
      </Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default MovieRouter;
