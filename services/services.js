import axios from "axios";

const apiUrl = "https://api.themoviedb.org/3";
const apiKey = "api_key=53f738c27706ccf618c76157f9841b66";
const language = "&language=";

export const getPopularMovies = async (region) => {
  const resp = await axios.get(
    `${apiUrl}/movie/popular?${apiKey}${language}${region}`
  );
  return resp.data.results;
};

// Get Upcoming Movies
export const getUpcomingMovies = async (region) => {
  const resp = await axios.get(
    `${apiUrl}/movie/upcoming?${apiKey}${language}${region}`
  );
  return resp.data.results;
};

// Get Popular TV
export const getPopularTv = async (region) => {
  const resp = await axios.get(
    `${apiUrl}/tv/popular?${apiKey}${language}${region}`
  );
  return resp.data.results;
};

// Get Family Movies
export const getFamilyMovies = async (region) => {
  const resp = await axios.get(
    `${apiUrl}/discover/movie?${apiKey}&with_genres=10751${language}${region}`
  );
  return resp.data.results;
};

// Get Documnetery Movies
export const getDocumentaryMovies = async (region) => {
  const resp = await axios.get(
    `${apiUrl}/discover/movie?${apiKey}&with_genres=99${language}${region}`
  );
  return resp.data.results;
};

// Get movie by id
export const getMovie = async (id, type = "movie", region = "fr") => {
  const response = await axios.get(
    `${apiUrl}/${type}/${id}?${apiKey}${language}${region}`
  );
  return response.data;
};

// Get atwork serie or movie
export const getAtworks = async (query, type) => {
  const response = await axios.get(
    `${apiUrl}/search/${type}?${apiKey}&query=${query}`
  );
  console.log(response);
  return response.data.results;
};
