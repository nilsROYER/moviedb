import React, {useEffect, useState} from "react";
import { Text, View } from 'react-native'
import StarRating from "react-native-star-rating";

const AtworkInfos = ({ infos, starRating, setStar }) => {
  const [star, setStarRating] = useState(0)

  useEffect(() => {
    setStarRating(starRating)
  }, [])

  return (
    <>
      { infos && infos.map(i => <Text>{i.label} : {i.info}</Text>)}
     <View>
        <Text>Note : {star} / 5</Text>
        <StarRating 
          disabled={false}
          maxStars={5}
          rating={star}
          selectedStar={(rating) => setStar(setStarRating(rating))}
        />
      </View>
    </>
  )
}
 export default AtworkInfos
