import React from "react";
import { Button } from 'react-native'

const BackButton = ({ title = 'Back', navigation }) => {
  return (
    <>
      <Button title={title} onPress={() => navigation.goBack()} />
    </>
  )
}

export default BackButton
