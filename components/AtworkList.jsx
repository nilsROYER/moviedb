import React from "react";
import { View, SectionList, Text, Button, StyleSheet } from 'react-native'

const AtworkList = ({ atworkInfos }) => {
  return (
    <>
      <View style={style.sectionList}>
        <SectionList
          sections={ atworkInfos }
          renderItem={({ item }) => (
            <Text style={style.item}> - {item.name}</Text>
          )}
          renderSectionHeader={({ section }) => (
            <Text style={style.sectionHeader}>{section.title} : </Text>
          )}
          keyExtractor={(item, index) => index}
        />
      </View>
    </>
  )
}

const style = StyleSheet.create({
  sectionList: {
    flex: 1,
    paddingTop: 22,
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontWeight: "bold",
    backgroundColor: "rgba(247, 247, 247, 1.0)",
  },
  item: {
    padding: 10,
    height: 44,
  },
});


export default AtworkList
