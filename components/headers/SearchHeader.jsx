import React from "react";
import { Feather } from "@expo/vector-icons"
import { TextInput, StyleSheet, TouchableOpacity, View } from "react-native";

const SearchHeader = () => {
  return (
    <View style={styles.container}>
      <TouchableOpacity>
        <Feather name="search" size={36} color="orange"/>
      </TouchableOpacity>
      <TextInput style={styles.input} placeholder="Rechercher un film"/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "baseline",
    flexWrap: "wrap"
  },
  input: {
    maxHeight: 20
  }
})

export default SearchHeader
