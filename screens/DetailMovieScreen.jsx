import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
} from "react-native";
import AtworkInfos from "../components/AtworkInfos.jsx";
import AtworkList from "../components/AtworkList.jsx";
import BackButton from "../components/buttons/BackButton.jsx";
import { getMovie } from "../services/services.js";

const DetailMovieScreen = ({ route, navigation, movieType }) => {
  const movieId = route.params.atworkDetail.id;
  const type = route.params.movieType
  const [movieDetail, setMovieDetail] = useState({});
  const [loaded, setLoaded] = useState(true);
  const [error, setError] = useState([]);
  const [starRating, setStarRating] = useState(0);

  const [atworkInfosList, setAtworkInfosList] = useState([])
  const [atworkInfos, setAtworkInfos] = useState([])

  useEffect(() => {
    getMovie(movieId, type)
      .then((movie) => {
        setMovieDetail(movie);
        setLoaded(false);
      })
      .catch((error) => {
        setError(error);
      });
    // setStarRating(Math.round(movieDetail.vote_average / 2));
  }, [movieId]);

  useEffect(() => {
    setStarRating(Math.round(movieDetail.vote_average / 2));
    switch(type) {
      default:
        setAtworkInfosList([
          { title: "Genres", data: movieDetail.genres },
          { title: "Companies", data: movieDetail.production_companies }
        ])
        setAtworkInfos([
          { label: "Dates de sortie", info: movieDetail.release_date }
        ])
        break;
      case "tv":
        setAtworkInfosList([
          { title: "Producteur", data: movieDetail.created_by },
          { title: "Genres", data: movieDetail.genres },
          { title: "Companies", data: movieDetail.production_companies },
        ])
        setAtworkInfos([
          { label: "Date de sortie", info: movieDetail.first_air_date },
          { label: "Episodes", info: movieDetail.episode_run_time },
        ])
        break;
    }
  }, [movieDetail]);

  return (
    <View style={style.container}>
      {!loaded && (
        <ScrollView>
          <Image
            resizeMode="cover"
            style={style.image}
            source={
              movieDetail.poster_path
                ? {
                    uri:
                      "https://image.tmdb.org/t/p/w500" +
                      movieDetail.poster_path,
                  }
                : placeholderImage
            }
          />
            <View>
              <Text>{movieDetail.title}</Text>
              <Text>Pays : {movieDetail.production_countries[0].name}</Text>
              <AtworkInfos infos={atworkInfos} starRating={starRating} setStar={setStarRating} />
              <View style={style.sectionList}>
                <AtworkList atworkInfos={atworkInfosList} />
              </View>
            </View>

          {/* <Text>{movieDetail.title}</Text> */}
          {/* <Text>Pays : {movieDetail.production_countries[0].name}</Text> */}
          {/* <AtworkInfos infos={atworkInfos} starRating={starRating} setStar={setStarRating} /> */}
          {/* <View style={style.sectionList}> */}
          {/*   <AtworkList atworkInfos={atworkInfosList} /> */}
          {/* </View> */}
          <BackButton navigation={navigation} />
        </ScrollView>
      )}
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    paddingTop: 75,
    paddingLeft: 5,
  },
  image: {
    height: 200,
    width: 125,
    borderRadius: 20,
  },
  sectionList: {
    flex: 1,
    paddingTop: 22,
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontWeight: "bold",
    backgroundColor: "rgba(247, 247, 247, 1.0)",
  },
  item: {
    padding: 10,
    height: 44,
  },
});

export default DetailMovieScreen;
