import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  ScrollView,
  ActivityIndicator,
  Button,
  TouchableOpacity,
} from "react-native";
import { SliderBox } from "react-native-image-slider-box";
import List from "../components/List";
import Error from "../components/Error";
import { Feather } from "@expo/vector-icons";

import {
  getPopularMovies,
  getUpcomingMovies,
  getPopularTv,
  getFamilyMovies,
  getDocumentaryMovies,
} from "../services/services";

const dimentions = Dimensions.get("screen");

const Home = ({ navigation }) => {
  const [moviesImages, setMoviesImages] = useState();
  const [popularMovies, setPopularMovies] = useState();
  const [popularTv, setPopularTv] = useState();
  const [familyMovies, setFamilyMovies] = useState();
  const [documentaryMovies, setDocumentaryMovies] = useState();

  const [error, setError] = useState(false);
  const [loaded, setLoaded] = useState(true);

  const defaultScreen = "DetailMovieScreen";

  const getData = () => {
    const region = "fr";
    return Promise.all([
      getUpcomingMovies(region),
      getPopularMovies(region),
      getPopularTv(region),
      getFamilyMovies(region),
      getDocumentaryMovies(region),
    ]);
  };

  useEffect(() => {
    getData()
      .then(
        ([
          upcomingMoviesData,
          popularMoviesData,
          popularTvData,
          familyMoviesData,
          documentaryMoviesData,
        ]) => {
          const moviesImagesArray = [];
          upcomingMoviesData.forEach((movie) => {
            moviesImagesArray.push(
              "https://image.tmdb.org/t/p/w500" + movie.poster_path
            );
          });

          setMoviesImages(moviesImagesArray);
          setPopularMovies(popularMoviesData);
          setPopularTv(popularTvData);
          setFamilyMovies(familyMoviesData);
          setDocumentaryMovies(documentaryMoviesData);
        }
      )
      .catch(() => {
        setError(true);
      })
      .finally(() => {
        setLoaded(false);
      });
  }, []);

  return (
    <View>
      {/* Upcoming Movies */}
      {!loaded && !error && (
        <ScrollView>
          {moviesImages && (
            <View style={styles.sliderContainer}>
              <SliderBox
                images={moviesImages}
                dotStyle={styles.sliderStyle}
                sliderBoxHeight={dimentions.height / 1.5}
                autoplay={true}
                circleLoop={true}
              />
            </View>
          )}
          {/*Search screen*/}
          <View style={styles.carousel}>
            <TouchableOpacity
              onPress={() => navigation.navigate("SearchMovieScreen")}
            >
            <Feather name="search" size={36} color="orange" />
            </TouchableOpacity>
          </View>
          {/* Popular Movies */}
          {popularMovies && (
            <View style={styles.carousel}>
              <List
                title={"Films populaires"}
                content={popularMovies}
                navigation={navigation}
                movieType="movie" 
              />
            </View>
          )}
          {/* Popular TV Shows */}
          {popularTv && (
            <View style={styles.carousel}>
              <List
                title={"Programmes TV"}
                content={popularTv}
                navigation={navigation}
                movieType="tv"
              />
            </View>
          )}
          {/* Family Movies */}
          {familyMovies && (
            <View style={styles.carousel}>
              <List
                title={"Films familiale"}
                content={familyMovies}
                navigation={navigation}
                movieType="movie" 
              />
            </View>
          )}
          {/* Documentary Movies */}
          {documentaryMovies && (
            <View style={styles.carousel}>
              <List
                title={"Reportage/Documentaire"}
                content={documentaryMovies}
                navigation={navigation}
                movieType="movie" 
              />
            </View>
          )}
        </ScrollView>
      )}
      {!loaded && <ActivityIndicator size="large" />}
      {error && <Error />}
    </View>
  );
};
const styles = StyleSheet.create({
  sliderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  sliderStyle: {
    height: 0,
  },
  carousel: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  searchLogo: {
    height: 36,
    width: 36,
  },
});

export default Home;
